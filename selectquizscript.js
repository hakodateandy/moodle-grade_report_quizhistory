$(document).ready(
    function () {

        // select all
        $( "#selectallsections" ).click(function(e) {
            e.preventDefault();
            var checkboxes = $('form').find(':checkbox');
            checkboxes.prop('checked', true);
        });

        // deselect all
        $( "#deselectallsections" ).click(function(e) {
            e.preventDefault();
            var checkboxes = $('form').find(':checkbox');
            checkboxes.prop('checked', false);
        });

        // select of sections.
        $('a[selectthissection]').click(function(e) {
            e.preventDefault();

            var sectionnumber = $(this).attr("selectthissection");
            var checkboxes = $('form').find(':checkbox[coursesection="'+sectionnumber+'"]');
            checkboxes.prop('checked', true);
        });

        // deselect of sections.
        $('a[deselectthissection]').click(function(e) {
            e.preventDefault();

            var sectionnumber = $(this).attr("deselectthissection");
            var checkboxes = $('form').find(':checkbox[coursesection="'+sectionnumber+'"]');
            checkboxes.prop('checked', false);
        });

    }
)