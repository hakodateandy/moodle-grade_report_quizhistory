var trnodes = $('.gradereport_quizhistory').find('tr');
trnodes.each(function( index ) {
  if (index != 0 && !$( this ).hasClass('emptyrow')) {
        // Note: index == 0 is the header row.

        var cellid = '#gradereport_quizhistory_r'+ (index - 1) +'_c0';
        var overlayid = 'qhoverlay_' + (index - 1);
        var overlay = "<div class=\"qhoverlay "+overlayid+"\">"+$(cellid).html()+"</div>";
        $('#page').append(overlay);

        // if background color is transparent or unset, then apply the body background.
        var cellbackgroundcolor = $(cellid).css('background-color');
        if (cellbackgroundcolor == 'undefined'
            || cellbackgroundcolor == ''
            || cellbackgroundcolor == 'transparent'
            || cellbackgroundcolor == 'rgba(0, 0, 0, 0)') {
            cellbackgroundcolor = 'white';
        }

        $('.'+overlayid).css({
            position: "absolute",
            width: $(cellid).outerWidth(),
            height: $(cellid).height(),
            'max-height': $(cellid).height(),
            top: $(cellid).position().top,
            left: $(cellid).position().left,
            'background-color': cellbackgroundcolor,
            'border-collapse': $(cellid).css('border-collapse'),
            'border-color': $(cellid).css('border-color'),
            'border-style': $(cellid).css('border-style'),
            'border-width': $(cellid).css('border-width'),
            color: $(cellid).css('color'),
            'font-family': $(cellid).css('font-family'),
            'font-size': $(cellid).css('font-size'),
            'line-height': $(cellid).css('line-height'),
            'padding-left': $(cellid).css('padding-left'),
            'padding-top': $(cellid).css('padding-top'),
            'text-align': $(cellid).css('text-align'),
            'vertical-align': $(cellid).css('vertical-align')
        });

        $(window).scroll(function(){

          if ($(window).scrollLeft() > $(cellid).offset().left) {
                var leftOffset = parseInt($("."+overlayid).css('left'));
                $(window).scroll(function(){
                    $('.'+overlayid).css({
                        left: $(this).scrollLeft(),
                        display: 'block'
                    });

                });

          }

          if ($(window).scrollLeft() < $(cellid).offset().left) {
              $(window).scroll(function(){
                    $('.'+overlayid).css({
                        display: 'none'
                    });

                });
          }

        });
  }
});
