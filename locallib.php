<?php

function quizhistoryreport_get_user_preferences($name = null, $default = null, $user = null) {
    global $USER, $DB;

    if (is_null($name)) {
        // All prefs.
    } else if (is_numeric($name)) {
        throw new coding_exception('Invalid preference name in quizhistoryreport_get_user_preferences() call');
    }

    if (is_null($user)) {
        $user = $USER;
    } else if (isset($user->id)) {
        // Is a valid object.
    } else if (is_numeric($user)) {
        $user = (object)array('id' => (int)$user);
    } else {
        throw new coding_exception('Invalid $user parameter in quizhistoryreport_get_user_preferences() call');
    }

    return $DB->get_field('report_quizhistory_userpref', 'value', array('userid' => $user->id, 'name' => $name));
}

function quizhistoryreport_set_user_preference($name, $value, $user = null) {
    global $USER, $DB;

    if (empty($name) or is_numeric($name)) {
        throw new coding_exception('Invalid preference name in quizhistoryreport_set_user_preference() call');
    }

    if (is_null($value)) {
        // Null means delete current.
        return quizhistoryreport_unset_user_preference($name, $user);
    } else if (is_object($value)) {
        throw new coding_exception('Invalid value in quizhistoryreport_set_user_preference() call, objects are not allowed');
    } else if (is_array($value)) {
        throw new coding_exception('Invalid value in quizhistoryreport_set_user_preference() call, arrays are not allowed');
    }

    if (is_null($user)) {
        $user = $USER;
    } else if (isset($user->id)) {
        // It is a valid object.
    } else if (is_numeric($user)) {
        $user = (object)array('id' => (int)$user);
    } else {
        throw new coding_exception('Invalid $user parameter in quizhistoryreport_set_user_preference() call');
    }

    if (empty($user->id) or isguestuser($user->id)) {
        // No permanent storage for not-logged-in users and guest.
        throw new coding_exception('No permanent storage for not-logged-in users and guest for quizhistoryreport_set_user_preference() call');
    }

    if ($preference = $DB->get_record('report_quizhistory_userpref', array('userid' => $user->id, 'name' => $name))) {
        if ($preference->value === $value) {
            // Preference already set to this value.
            return true;
        }
        $DB->set_field('report_quizhistory_userpref', 'value', $value, array('id' => $preference->id));

    } else {
        $preference = new stdClass();
        $preference->userid = $user->id;
        $preference->name   = $name;
        $preference->value  = $value;
        $DB->insert_record('report_quizhistory_userpref', $preference);
    }

    return true;
}

function quizhistoryreport_unset_user_preference($name, $user = null) {
    global $USER, $DB;

    if (empty($name) or is_numeric($name)) {
        throw new coding_exception('Invalid preference name in quizhistoryreport_unset_user_preference() call');
    }

    if (is_null($user)) {
        $user = $USER;
    } else if (isset($user->id)) {
        // It is a valid object.
    } else if (is_numeric($user)) {
        $user = (object)array('id' => (int)$user);
    } else {
        throw new coding_exception('Invalid $user parameter in quizhistoryreport_unset_user_preference() call');
    }

    if (empty($user->id) or isguestuser($user->id)) {
        // No permanent storage for not-logged-in user and guest.
        throw new coding_exception('No permanent storage for not-logged-in users and guest for quizhistoryreport_unset_user_preference() call');
        return true;
    }

    // Delete from DB.
    $DB->delete_records('report_quizhistory_userpref', array('userid' => $user->id, 'name' => $name));

    return true;
}