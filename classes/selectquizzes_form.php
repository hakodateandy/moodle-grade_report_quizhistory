<?php

/**
 * Form for select quizzes
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace gradereport_quizhistory;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * Form for select quizzes
 *
 * @since      Moodle 2.8
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class selectquizzes_form extends \moodleform {

    /**
     * Definition of the Mform for filters displayed in the report.
     */
    public function definition() {
        global $DB;

        $mform    = $this->_form;
        $course   = $this->_customdata['course'];
        $quizzes  = $this->_customdata['quizzes']; //not used anymore, we now retriev them from the cached get_fast_modinfo.
        $selectedquizzes = $this->_customdata['selectedquizzes'];

        // sort the quizzes
        $modinfo = get_fast_modinfo($course->id);
        $currentsection = 0;
        foreach ($modinfo->get_cms() as $cmid => $cm) {

            if ($cm->modname == 'quiz') {

                if ($cm->section !== $currentsection) {
                    $currentsection = $cm->section;

                    $section = $DB->get_record('course_sections', array('course' => $course->id, 'id' => $currentsection));

                    if (!empty($section->name)) {
                        $mform->addElement('static', $cm->name, '<strong>'. format_string($section->name) .
                        ' (<a href="" selectthissection="'.$currentsection.'">Select</a> / <a href="" deselectthissection="'.$currentsection.'">Deselect All</a>)' . '</strong>');
                    }
                }

                // // retrieve the grade item id.
                $gradeitemid = $DB->get_field('grade_items', 'id',
                    array('iteminstance' => $cm->instance, 'courseid' => $course->id, 'itemtype' => 'mod', 'itemmodule' => 'quiz'));
                $mform->addElement('checkbox', 'quiz'.$gradeitemid, '', format_string($cm->name), array('coursesection' => $currentsection));
            }
        }

        $mform->addElement('hidden', 'id', $course->id);
        $mform->setType('id', PARAM_INT);

        // Add a submit button.
        $mform->addElement('submit', 'submitbutton', get_string('savepreferences', 'gradereport_quizhistory'));
    }

    /**
     * Form validation
     *
     * @param array $data data from the form.
     * @param array $files files uploaded.
     *
     * @return array of errors.
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Check that at least one quiz has been checked.
        $foundquiz = false;
        foreach($data as $dataid => $datavalue) {
            if (strpos($dataid, 'quiz') === 0) {
                $foundquiz = true;
            }
        }

        if (!$foundquiz) {
            $quizzes  = $this->_customdata['quizzes'];
            $errors['quiz'.key($quizzes)] = get_string('errornoquizzes', 'gradereport_quizhistory');
        }

        return $errors;
    }

}
