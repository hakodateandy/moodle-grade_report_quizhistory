<?php

/**
 * Renderer for quizhistory grade report.
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace gradereport_quizhistory\output;

defined('MOODLE_INTERNAL') || die;

/**
 * Renderer for quizhistory grade report.
 *
 * @since      Moodle 2.8
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class renderer extends \plugin_renderer_base {

    /**
     * Get the html for the table.
     *
     * @param tablelog $tablelog table object.
     *
     * @return string table html
     */
    protected function render_tablelog(tablelog $tablelog) {
        $o = '';
        ob_start();
        $tablelog->out($tablelog->pagesize, false);
        $o = ob_get_contents();
        ob_end_clean();

        return $o;
    }

}
