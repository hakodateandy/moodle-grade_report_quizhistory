<?php

/**
 * Renderable class for gradequizhistory report.
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace gradereport_quizhistory\output;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/tablelib.php');

require_once($CFG->dirroot . '/grade/report/quizhistory/locallib.php');

/**
 * Renderable class for gradequizhistory report.
 *
 * @since      Moodle 2.8
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class tablelog extends \table_sql implements \renderable {

    /**
     * @var int course id.
     */
    protected $courseid;

    protected $selectedquizzes;

    /**
     * @var \context context of the page to be rendered.
     */
    protected $context;

    /**
     * @var \stdClass A list of filters to be applied to the sql query.
     */
    protected $filters;

    /**
     * @var array A list of grade items present in the course.
     */
    protected $gradeitems = array();

    /**
     * @var \course_modinfo|null A list of cm instances in course.
     */
    protected $cms;

    /**
     * @var int The default number of decimal points to use in this course
     * when a grade item does not itself define the number of decimal points.
     */
    protected $defaultdecimalpoints;

    public $totalentries;

    /**
     * Sets up the table_log parameters.
     *
     * @param string $uniqueid unique id of table.
     * @param \context_course $context Context of the report.
     * @param \moodle_url $url url of the page where this table would be displayed.
     * @param array $filters options are:
     *                          itemid : limit to specific grade item (default: all)
     *                          datefrom : start of date range
     *                          datetill : end of date range
     *                          format : page | csv | excel (default: page)
     * @param string $download Represents download format, pass '' no download at this time.
     * @param int $page The current page being displayed.
     * @param int $perpage Number of rules to display per page.
     */
    public function __construct($uniqueid, \context_course $context, $url, $filters = array(), $download = '', $page = 0,
                                $perpage = 100) {
        global $CFG;

        parent::__construct($uniqueid);

        $this->set_attribute('class', 'gradereport_quizhistory generaltable generalbox');

        // Set protected properties.
        $this->context = $context;
        $this->courseid = $this->context->instanceid;
        $this->pagesize = $perpage;
        $this->page = $page;
        $this->filters = (object)$filters;
        $this->gradeitems = \grade_item::fetch_all(array('courseid' => $this->courseid));
        $this->cms = get_fast_modinfo($this->courseid);
        $this->useridfield = 'userid';
        $this->defaultdecimalpoints = grade_get_setting($this->courseid, 'decimalpoints', $CFG->grade_decimalpoints);

        // Define columns in the table.
        $this->define_table_columns();

        // Define configs.
        $this->define_table_configs($url);

        // Set download status.
        $this->is_downloading($download, get_string('exportfilename', 'gradereport_quizhistory'));
    }

    /**
     * Define table configs.
     *
     * @param \moodle_url $url url of the page where this table would be displayed.
     */
    protected function define_table_configs(\moodle_url $url) {

        // Set table url.
        $urlparams = (array)$this->filters;
        unset($urlparams['submitbutton']);
        $url->params($urlparams);
        $this->define_baseurl($url);

        // Set table configs.
        $this->collapsible(true);
        $this->pageable(true);
    }

    /**
     * Setup the headers for the html table.
     */
    protected function define_table_columns() {
        global $COURSE, $DB;
        $extrafields = get_extra_user_fields($this->context);

        // Define headers and columns.
        $cols = array(
            'fullname' => get_string('name'),
        );

        // Add headers for extra user fields.
        foreach ($extrafields as $field) {
            if (get_string_manager()->string_exists($field, 'moodle')) {
                $cols[$field] = get_string($field);
            } else {
                $cols[$field] = $field;
            }
        }

        // Add Pass failed column.
        $cols['allpassed'] = get_string('allpassed', 'gradereport_quizhistory');
        $cols['passed'] = get_string('passed', 'gradereport_quizhistory');
        $cols['failed'] = get_string('failed', 'gradereport_quizhistory');

        // Add selected quizzes.
        $selectedquizzes = quizhistoryreport_get_user_preferences('grade_report_quizhistoryquizzes_' . $COURSE->id);
        if (!empty($selectedquizzes)) { // the user preferences are only empty the first time, then display all grade items just for this time...
            $selectedquizzes = unserialize($selectedquizzes);
            $this->selectedquizzes = $selectedquizzes;
            $selectedq = array();
            foreach ($selectedquizzes as $selectedquiz => $enabled) {
                // Find quiz name.
                $itemid = str_replace('quiz', '', $selectedquiz);
                // $gradeitem = $DB->get_record('grade_items', array('id' => $itemid));
                $selectedq[$selectedquiz] = $this->gradeitems[$itemid]->get_name();
            }
            // Add remaining headers.
            $cols = array_merge($cols, $selectedq);
        }

        $this->define_columns(array_keys($cols));
        $this->define_headers(array_values($cols));
    }

    /**
     * You can override this method in a child class. See the description of
     * build_table which calls this method.
     */
    function other_cols($column, $row) {
        global $CFG;

        if ($column == 'email') { return $row->email; }
        if ($column == 'allpassed') {
            if (!$this->is_downloading()) {
                return '<span style="color:green">' . $row->allpassed . '</span>';
            } else {
                return $row->allpassed;
            }
        }
        if ($column == 'passed') {
            if (!$this->is_downloading()) {
                return '<span style="color:green">' . $row->passed . '</span>';
            } else {
                return $row->passed;
            }
        }
        if ($column == 'failed') {
             if (!$this->is_downloading()) {
                return '<span style="color:red">' . $row->failed . '</span>';
            } else {
                return $row->failed;
            }
        }
        if (array_key_exists($column, $this->selectedquizzes)) {

            if(!isset($row->{$column}->finalgrade)) {return'';}
             $itemid = str_replace('quiz', '', $column);
             if (!$this->is_downloading()) {
                if ((($row->{$column}->finalgrade / $row->{$column}->grademax) * 100) > $row->{$column}->gradepass) {
                    $color = 'green';
                } else {
                    $color = 'red';
                }

                $cm = $this->cms->instances['quiz'][$row->{$column}->iteminstance];
                return '<a href="'.$CFG->httpswwwroot.'/grade/report/quizhistory/redirect_to_attempt.php?cmid='.$cm->id.'&userid='.$row->userid.'"
                    style="color:'.$color.'" target="_blank">' . $row->{$column}->finalgrade . '</a>';
             } else {
                return $row->{$column}->finalgrade;
             }
        }
        return '';
    }

    /**
     * Get the SQL fragment to sort by.
     *
     * This is overridden to sort by timemodified and ID by default. Many items happen at the same time
     * and a second sorting by ID is valuable to distinguish the order in which the quizhistory happened.
     *
     * @return string SQL fragment.
     */
    public function get_sql_sort() {
        $columns = $this->get_sort_columns();
        if (count($columns) == 1 && isset($columns['timemodified']) && $columns['timemodified'] == SORT_DESC) {
            // Add the 'id' column when we are using the default sorting.
            $columns['id'] = SORT_DESC;
            return self::construct_order_by($columns);
        }
        return parent::get_sql_sort();
    }


    /**
     * Query the reader. Store results in the object for use by build_table.
     *
     * @param int $pagesize size of page for paginated displayed table.
     * @param bool $useinitialsbar do you want to use the initials bar.
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        global $DB, $COURSE, $SESSION;

        // REFACTOR SQL QUERY
        $coursecontext = $this->context;

        $params = array(
            'courseid' => $coursecontext->instanceid,
        );
        $where = '';
        $groupid = $SESSION->quizhistoryreport['filtergroupid'];
        if (!empty($groupid)) {
            $where .= 'AND u.id IN (SELECT gm.userid FROM {groups_members} gm WHERE gm.groupid = :gr_grpid) ';
            $params += array('gr_grpid' => $groupid);
        }

        if (!empty($SESSION->quizhistoryreport['filterfirstname'])) {
            $where .= " AND UPPER(u.firstname) LIKE :firstname";
            $params += array('firstname' => $SESSION->quizhistoryreport['filterfirstname']. '%');
        }

        if (!empty($SESSION->quizhistoryreport['filtersurname'])) {
            $where .= " AND UPPER(u.lastname) LIKE :surname";
            $params += array('surname' => $SESSION->quizhistoryreport['filtersurname']. '%');
        }
        // Filter with the selected quiz only.
        $selectedquizzes = quizhistoryreport_get_user_preferences('grade_report_quizhistoryquizzes_' . $COURSE->id);
        $gradeitemids = array();
        if (!empty($selectedquizzes)) { // the user preferences are only empty the first time, then display all grade items just for this time...
            $selectedquizzes = unserialize($selectedquizzes);
            foreach ($selectedquizzes as $selectedquiz => $enabled) { 
                $gradeitemids[] = str_replace('quiz', '', $selectedquiz);
            }
        }

        $convertedhistories = array();
        foreach ($gradeitemids as $gradeitemid) {
            $sql = 'SELECT u.email, u.firstname,u.lastname, 
                           ggh.userid, ggh.timemodified, ggh.id, gi.gradepass, ggh.finalgrade, 
                           gi.grademax, gi.iteminstance, gi.itemname
                        FROM {grade_grades_history} ggh
                        JOIN {grade_items} gi ON gi.id = ggh.itemid
                        JOIN {user} u ON u.id = ggh.userid 
                        LEFT OUTER JOIN {grade_grades_history} ggh2 ON (ggh.id = ggh2.id and ggh.timemodified < ggh2.timemodified)
                        where ggh2.id is null 
                            AND gi.courseid = :courseid 
                            AND gi.id IN ('.$gradeitemid.') 
                            AND ggh.finalgrade IS NOT NULL
                            '. $where.'  
                        order by ggh.id
                        ';
            $newhistoriestmp = $DB->get_records_sql($sql, $params);

            foreach ($newhistoriestmp as $history) {
                if (empty($convertedhistories[$history->userid])) {
                    $convertedhistories[$history->userid] = new \stdClass();
                    $convertedhistories[$history->userid]->passed = 0;
                    $convertedhistories[$history->userid]->failed = 0;
                    $convertedhistories[$history->userid]->allpassed = 0;
                    $convertedhistories[$history->userid]->userid = $history->userid;
                    $convertedhistories[$history->userid]->email = $history->email;
                    $convertedhistories[$history->userid]->firstname = $history->firstname;
                    $convertedhistories[$history->userid]->lastname = $history->lastname;
                }

                $quizid = 'quiz'.$gradeitemid;
               
                $history->finalgrade = round($history->finalgrade);

                $convertedhistories[$history->userid]->{$quizid} = new \stdClass();
                $convertedhistories[$history->userid]->{$quizid}->timemodified = $history->timemodified;
                $convertedhistories[$history->userid]->{$quizid}->itemname = $history->itemname;
                $convertedhistories[$history->userid]->{$quizid}->finalgrade = $history->finalgrade;
                $convertedhistories[$history->userid]->{$quizid}->grademax = $history->grademax;
                $convertedhistories[$history->userid]->{$quizid}->gradepass = $history->gradepass;
                $convertedhistories[$history->userid]->{$quizid}->iteminstance = $history->iteminstance;
            }
        }

        // Calculate the number of passed/failed.
        $userwithoutgradedquiz = array();
        $userquiztounset = array();
        foreach ($convertedhistories as $index => $convertedhistory) {
            $attemptedaquiz = false;
            foreach($this->columns as $columnanme => $position) {
                if (strpos($columnanme, 'quiz') === 0) { //only check quizX column
                    // Increment passed/failed counters for this user.
                    $quizattempted = false;
                    if (isset($convertedhistory->{$columnanme}->finalgrade)) {
                        if ((($convertedhistory->{$columnanme}->finalgrade / $convertedhistory->{$columnanme}->grademax) * 100)
                                    > $convertedhistory->{$columnanme}->gradepass) {
                            $convertedhistories[$convertedhistory->userid]->allpassed += 1;

                            if ((empty($this->filters->datefrom) or ($convertedhistory->{$columnanme}->timemodified >= $this->filters->datefrom))
                                 and (empty($this->filters->datetill) or ($convertedhistory->{$columnanme}->timemodified <= $this->filters->datetill))) {
                                $convertedhistories[$convertedhistory->userid]->passed += 1;
                                // indicate that the user has attempted at least one quiz.
                                $quizattempted = true;
                            }
                        } else {
                            if ((empty($this->filters->datefrom) or ($convertedhistory->{$columnanme}->timemodified >= $this->filters->datefrom))
                                 and (empty($this->filters->datetill) or ($convertedhistory->{$columnanme}->timemodified <= $this->filters->datetill))) {
                                $convertedhistories[$convertedhistory->userid]->failed += 1;
                                // indicate that the user has attempted at least one quiz.
                                $quizattempted = true;
                            }
                        }
                    }

                    //Remove this quiz from the user quiz attempt (to apply period filter)
                    if ($quizattempted) {
                        $attemptedaquiz = true;
                    } else {
                        $userquiztounsets[$index][] = $columnanme;
                    }
                }
            }

            // If the user didn't have any graded attempt then we will need to remove it later from the result (it is likely a teacher attempt).
            if (empty($attemptedaquiz)) {
                $userwithoutgradedquiz[] = $index;
            }
        }

        // Sanitize: remove people who didn't get any grade in any quiz.
        if (!empty($userwithoutgradedquiz)) {
            foreach ($userwithoutgradedquiz as $indextoremove) {
                unset($convertedhistories[$indextoremove]);
            }
        }
        // Sanitize: remove quiz grade that are outside the time period filter.
        if (!empty($userquiztounsets)) {
            foreach($userquiztounsets as $index => $columnnames) {
                foreach($columnnames as $columnname) {
                    unset($convertedhistories[$index]->{$columnname});
                }
            }
        }

        // Sort the entries
        $tsort = optional_param('tsort', 0, PARAM_ALPHANUMEXT);
        // Retrieve the current sorting store in the user SESSION
        if (isset($SESSION->quizhistoryreport['sortingpriorities'])) {
            $sortingpriorities = $SESSION->quizhistoryreport['sortingpriorities'];
            $sorting = $SESSION->quizhistoryreport['sorting'];
        } else {
            $sortingpriorities = array();
            $sorting = array();
        }
        // record the new sorting column
        if (!empty($tsort)) {

            // set the user preference.
            if (!isset($sorting[$tsort])) {
                // the user never sorted this column before
                $sorting[$tsort] = 'ASC';

            } else {
                // change the sorting order of the column.
                if ($sorting[$tsort] == 'ASC') {
                    $sorting[$tsort] = 'DESC';
                } else {
                    $sorting[$tsort] = 'ASC';
                }

                // as we are going to push this new sorting column as the last one to sort, we need to remove from its current position.
                if(($key = array_search($tsort, $sortingpriorities)) !== false) {
                    unset($sortingpriorities[$key]);
                }
            }

            // reorder to the priorities
            array_push($sortingpriorities, $tsort);

            // Record the new sorting priorities (which column had to be sorted last)
            $SESSION->quizhistoryreport['sortingpriorities'] = $sortingpriorities;
            $SESSION->quizhistoryreport['sorting'] = $sorting;
        }

        // We can now sort $convertedhistories
        foreach($sortingpriorities as $sortingcol) {

            if (isset($this->columns[$sortingcol]) or $sortingcol == 'firstname' or $sortingcol == 'lastname') {
                $SESSION->quizhistoryreport['currentsortingcol'] = $sortingcol;
                uasort($convertedhistories, 'gradereport_quizhistory\output\sort_by_order');
            }
        }
     
        $total = count($convertedhistories);
        $this->totalentries = $total;
        
           // When $pagesize == -1 then return total ('All')
        if ($pagesize == -1) {
            $pagesize = $total;
        }

        $this->pagesize($pagesize, $total);

        // Only retrieve a part of the entries if not downloading.
        if (!$this->is_downloading()) {
            $convertedhistories = array_slice($convertedhistories, $this->pagesize * $this->page, $this->pagesize);
        }

        foreach ($convertedhistories as $quizhistory) {
            $this->rawdata[] = $quizhistory;
        }

        // Set initial bars.
        if ($useinitialsbar) {
            $this->initialbars($total > $pagesize);
        }
    }
}

/**
 * Test function to check memory usage.
 */
function sizeofvar($var) {
    $start_memory = memory_get_usage();
    $var = unserialize(serialize($var));
    return memory_get_usage() - $start_memory - PHP_INT_SIZE * 8;
}

/**
 * Function use by php sorting function uasort
 */
function sort_by_order($a, $b) {
    global $SESSION;
    //new to check the tsort to apply the correct comparaison
    $currentsortingcol = $SESSION->quizhistoryreport['currentsortingcol'];
    switch ($currentsortingcol) {
        case 'firstname':
        case 'lastname':
        case 'email':
            if ($SESSION->quizhistoryreport['sorting'][$currentsortingcol] == 'ASC') {

                return strnatcasecmp($a->{$currentsortingcol}, $b->{$currentsortingcol});
            } else {
                return strnatcasecmp($b->{$currentsortingcol}, $a->{$currentsortingcol});
            }
            break;
        case 'passed':
        case 'failed':
        case 'allpassed':
            if ($SESSION->quizhistoryreport['sorting'][$currentsortingcol] == 'ASC') {
                return $a->{$currentsortingcol} - $b->{$currentsortingcol};
            } else {
                return $b->{$currentsortingcol} - $a->{$currentsortingcol};
            }
            break;
        default:
            if ($SESSION->quizhistoryreport['sorting'][$currentsortingcol] == 'ASC') {
                if (!isset($a->{$currentsortingcol}->finalgrade)) {
                    if (!isset($b->{$currentsortingcol}->finalgrade)) {
                        return 0;
                    } else {
                        // consider that the NULL field is top score
                        return 1000000 - $b->{$currentsortingcol}->finalgrade;
                    }
                 } else {
                    if (!isset($b->{$currentsortingcol}->finalgrade)) {
                        // consider that the NULL field is top score
                        return $a->{$currentsortingcol}->finalgrade - 1000000;
                    }
                 }
                return $a->{$currentsortingcol}->finalgrade - $b->{$currentsortingcol}->finalgrade;
            } else {
                if (!isset($b->{$currentsortingcol}->finalgrade)) {
                    if (!isset($a->{$currentsortingcol}->finalgrade)) {
                        return 0;
                    } else {
                        return -($a->{$currentsortingcol}->finalgrade);
                    }
                 } else {
                    if (!isset($a->{$currentsortingcol}->finalgrade)) {
                        return $b->{$currentsortingcol}->finalgrade;
                    }
                 } 
                return $b->{$currentsortingcol}->finalgrade - $a->{$currentsortingcol}->finalgrade;
            }
            break;
    }
}