<?php

/**
 * Form for grade quizhistory filters
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace gradereport_quizhistory;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * Form for grade quizhistory filters
 *
 * @since      Moodle 2.8
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class filter_form extends \moodleform {

    /**
     * Definition of the Mform for filters displayed in the report.
     */
    public function definition() {

        $mform    = $this->_form;
        $course   = $this->_customdata['course'];
        $itemids  = $this->_customdata['itemids'];
        $selectedquizzes = $this->_customdata['selectedquizzes'];

        $mform->addElement('static', 'selectedquizzes', get_string('selectedquizzes', 'gradereport_quizhistory'), $selectedquizzes);

        $mform->addElement('date_time_selector', 'datefrom', get_string('datefrom', 'gradereport_quizhistory'), array('optional' => true));
        $mform->addElement('date_time_selector', 'datetill', get_string('dateto', 'gradereport_quizhistory'), array('optional' => true));

        $mform->addElement('select', 'perpage', get_string('perpage', 'gradereport_quizhistory'),
            array('30' => 30, '50' => 50, '100' => 100, '-1' => 'All'));
        $mform->setType('perpage', PARAM_INT);

        $mform->addElement('hidden', 'id', $course->id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'userids');
        $mform->setType('userids', PARAM_SEQUENCE);

        // Add a submit button.
        $mform->addElement('submit', 'submitbutton', get_string('submit'));
    }

    /**
     * This method implements changes to the form that need to be made once the form data is set.
     */
    public function definition_after_data() {
        $mform = $this->_form;

    }

}
