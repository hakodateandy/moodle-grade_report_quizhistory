<?php

/**
 * Grade quizhistory report viewed event.
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace gradereport_quizhistory\event;

defined('MOODLE_INTERNAL') || die();

/**
 * Grade quizhistory report viewed event class.
 *
 * @package    gradereport_quizhistory
 * @since      Moodle 2.8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class grade_report_viewed extends \core\event\grade_report_viewed {

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('eventgradereportviewed', 'gradereport_quizhistory');
    }
}
