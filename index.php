<?php

/**
 * The gradebook grade quizhistory report
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->dirroot.'/grade/lib.php');

$download      = optional_param('download', '', PARAM_ALPHA);
$courseid      = required_param('id', PARAM_INT);        // Course id.
$page          = optional_param('page', 0, PARAM_INT);   // Active page.

// get/set perpage value.
$perpage = optional_param('perpage', null, PARAM_INT);   // entries displayed per page.
if (empty($perpage)) {
    $userperpage = get_user_preferences('grade_report_quizhistoryperpage_' . $courseid);

    if (!empty($userperpage)) {
        $perpage = $userperpage;
    } else {
        // default when no perpage has even been set.
        $perpage = 30;
    }
} else {
    set_user_preference('grade_report_quizhistoryperpage_' . $courseid, $perpage);
}

// Set the firstname/lastname.
$quizhistoryreportsifirst  = optional_param('sifirst', null, PARAM_NOTAGS);
$quizhistoryreportsilast   = optional_param('silast', null, PARAM_NOTAGS);
if (isset($quizhistoryreportsifirst)) {
    $SESSION->quizhistoryreport['filterfirstname'] = $quizhistoryreportsifirst;
} else {
    if (empty($SESSION->quizhistoryreport['filterfirstname'])) {
        $SESSION->quizhistoryreport['filterfirstname'] = '';
    }
}
if (isset($quizhistoryreportsilast)) {
    $SESSION->quizhistoryreport['filtersurname'] = $quizhistoryreportsilast;
} else {
    if (empty($SESSION->quizhistoryreport['filtersurname'])) {
        $SESSION->quizhistoryreport['filtersurname'] = '';
    }
}

// Set group.
$groupid  = optional_param('group', null, PARAM_INT);
if (isset($groupid)) {
    $SESSION->quizhistoryreport['filtergroupid'] = $groupid;
} else {
    if (empty($SESSION->quizhistoryreport['filtergroupid'])) {
        $SESSION->quizhistoryreport['filtergroupid'] = 0;
    }
}

// get/set dateform value.
$tsort = optional_param('tsort', 0, PARAM_ALPHANUMEXT);
$tshow = optional_param('tshow', null, PARAM_ALPHANUMEXT);
$thide = optional_param('thide', null, PARAM_ALPHANUMEXT);
$formpage = optional_param('page', null, PARAM_ALPHANUMEXT);
if (empty($download) and empty($tsort) and !isset($tshow) and !isset($thide) and !isset($formpage)) {
    $datefrom = optional_param_array('datefrom', 0, PARAM_RAW);
    if (empty($datefrom)) {
        $perpagetest = optional_param('perpage', null, PARAM_INT); // we check perpage existing as datefrom is just not sent when submitting...
        if (empty($perpagetest)) { // in this case we arrive on the page from a different page
            $userdateform = get_user_preferences('grade_report_quizhistorydateform_' . $courseid);
            if (!empty($userdateform)) {
                $datefrom = $userdateform;
            } else {
                $datefrom = 0;
            }
        } else { // we are setting the field to 0.
            set_user_preference('grade_report_quizhistorydateform_' . $courseid, 0);
        }
    } else {
        set_user_preference('grade_report_quizhistorydateform_' . $courseid,
            strtotime($datefrom['day'] . "-" . $datefrom['month'] . "-" . $datefrom['year']));
    }

// get/set datetill value.
    $datetill = optional_param_array('datetill', 0, PARAM_RAW);
    if (empty($datetill)) {
        $perpagetest = optional_param('perpage', null, PARAM_INT); // we check perpage existing as datetill is just not sent when submitting...
        if (empty($perpagetest)) { // in this case we arrive on the page from a different page
            $userdatetill = get_user_preferences('grade_report_quizhistorydatetill_' . $courseid);
            if (!empty($userdatetill)) {
                $datetill = $userdatetill;
            } else {
                $datetill = 0;
            }
        } else { // we are setting the field to 0.
            set_user_preference('grade_report_quizhistorydatetill_' . $courseid, 0);
        }
    } else {
        set_user_preference('grade_report_quizhistorydatetill_' . $courseid,
            strtotime($datetill['day'] . "-" . $datetill['month'] . "-" . $datetill['year']));
    }
} else {
    $datefrom = optional_param('datefrom', 0, PARAM_INT);
    $datetill = optional_param('datetill', 0, PARAM_INT);
}

$PAGE->set_pagelayout('report');
$url = new moodle_url('/grade/report/quizhistory/index.php', array('id' => $courseid));
$PAGE->set_url($url);
$PAGE->requires->css('/grade/report/quizhistory/indexscript.css');
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot . "/grade/report/quizhistory/indexscript.js"));

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
require_login($course);
$context = context_course::instance($course->id);

require_capability('gradereport/quizhistory:view', $context);
require_capability('moodle/grade:viewall', $context);

// Last selected report session tracking.
if (!isset($USER->grade_last_report)) {
    $USER->grade_last_report = array();
}
$USER->grade_last_report[$course->id] = 'quizhistory';

$select = "itemtype <> 'course' AND courseid = :courseid AND " . $DB->sql_isnotempty('grade_items', 'itemname', true, true);
$itemids = $DB->get_records_select_menu('grade_items', $select, array('courseid' => $course->id), 'itemname ASC', 'id, itemname');
$itemids = array(0 => get_string('allgradeitems', 'gradereport_quizhistory')) + $itemids;

$popupurl = new moodle_url('/grade/report/quizhistory/selectquizzes.php', array('id' => $courseid));
$selectedquizzes = $OUTPUT->action_link($popupurl, '<button>' . get_string('seeselectquizzes', 'gradereport_quizhistory') . '</button>',
    new popup_action('click', $popupurl, 'selectquizzes',
        array('height' => 800, 'width' => 650)),
    array('title' => get_string('selectquizzes', 'gradereport_quizhistory')));

$output = $PAGE->get_renderer('gradereport_quizhistory');
$params = array('course' => $course, 'itemids' => $itemids, 'selectedquizzes' => $selectedquizzes);
$mform = new \gradereport_quizhistory\filter_form(null, $params);
$filters = array();
if ($data = $mform->get_data()) {
    $filters = (array)$data;

    if (!empty($filters['datetill'])) {
        $filters['datetill'] += DAYSECS - 1; // Set to end of the chosen day.
    }
} else {
    $filters = array(
        'id' => $courseid,
        'itemid' => optional_param('itemid', 0, PARAM_INT),
        'datefrom' => $datefrom,
        'datetill' => $datetill,
        'perpage' => $perpage,
        'selectedquizzes' => $selectedquizzes
    );
}

$table = new \gradereport_quizhistory\output\tablelog('gradereport_quizhistory', $context, $url, $filters, $download, $page, $perpage);

$params = array('course' => $course, 'itemids' => $itemids, 'selectedquizzes' => $selectedquizzes);
$mform = new \gradereport_quizhistory\filter_form(null, $params);
$mform->set_data($filters);

if ($table->is_downloading()) {
    // Download file and exit.
    \core\session\manager::write_close();
    echo $output->render($table);
    die();
}

// Print header.
print_grade_page_head($COURSE->id, 'report', 'quizhistory', get_string('pluginname', 'gradereport_quizhistory'), false, '');
$mform->display();

// User search
$url = new moodle_url('/grade/report/quizhistory/index.php', array('id' => $course->id));

echo groups_print_course_menu($course, $url, true);

$tablehtml = $output->render($table);

// Check how many student in the course / groups.

if (empty($SESSION->quizhistoryreport['filtergroupid'])) {
    $groups = '';
} else {
    $groups = array($SESSION->quizhistoryreport['filtergroupid']);
}
$allusers = get_users_by_capability($context, 'mod/quiz:attempt', '', '', '', '', $groups);


echo user_search($url, $SESSION->quizhistoryreport['filterfirstname'], $SESSION->quizhistoryreport['filtersurname'], $table->totalentries, count($allusers));

// Render table.
echo $tablehtml;

$event = \gradereport_quizhistory\event\grade_report_viewed::create(
    array(
        'context' => $context,
        'courseid' => $courseid
    )
);
$event->trigger();

echo $OUTPUT->footer();

/**
 * COPY OF THE CORE USER_SEARCH without the heading.
 *
 * @param $url
 * @param $firstinitial
 * @param $lastinitial
 * @return string
 * @throws coding_exception
 */
function user_search($url, $firstinitial, $lastinitial, $usercount, $totalcount, $heading = null) {
    global $OUTPUT;

    $strall = get_string('all');
    $alpha  = explode(',', get_string('alphabet', 'langconfig'));

    if (!isset($heading)) {
        $heading = get_string('allparticipants');
    }

    $content = html_writer::start_tag('form', array('action' => new moodle_url($url)));
    $content .= html_writer::start_tag('div');

    // Search utility heading.
    $content .= $OUTPUT->heading(get_string('students').': '.$usercount.'/'.$totalcount, 3);

    // Bar of first initials.
    $content .= html_writer::start_tag('div', array('class' => 'initialbar firstinitial'));
    $content .= html_writer::label(get_string('firstname').' : ', null);

    if (!empty($firstinitial)) {
        $content .= html_writer::link($url.'&sifirst=', $strall);
    } else {
        $content .= html_writer::tag('strong', $strall);
    }

    foreach ($alpha as $letter) {
        if ($letter == $firstinitial) {
            $content .= html_writer::tag('strong', $letter);
        } else {
            $content .= html_writer::link($url.'&sifirst='.$letter, $letter);
        }
    }
    $content .= html_writer::end_tag('div');

    // Bar of last initials.
    $content .= html_writer::start_tag('div', array('class' => 'initialbar lastinitial'));
    $content .= html_writer::label(get_string('lastname').' : ', null);

    if (!empty($lastinitial)) {
        $content .= html_writer::link($url.'&silast=', $strall);
    } else {
        $content .= html_writer::tag('strong', $strall);
    }

    foreach ($alpha as $letter) {
        if ($letter == $lastinitial) {
            $content .= html_writer::tag('strong', $letter);
        } else {
            $content .= html_writer::link($url.'&silast='.$letter, $letter);
        }
    }
    $content .= html_writer::end_tag('div');

    $content .= html_writer::end_tag('div');
    $content .= html_writer::tag('div', '&nbsp;');
    $content .= html_writer::end_tag('form');

    return $content;
}
