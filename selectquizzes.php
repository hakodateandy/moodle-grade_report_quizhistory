<?php

/**
 * The "select quizzes" page for the quizhistory report
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->dirroot.'/grade/report/quizhistory/classes/selectquizzes_form.php');
require_once($CFG->dirroot . '/grade/report/quizhistory/locallib.php');

$courseid      = required_param('id', PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
require_login($course);
$context = context_course::instance($course->id);

require_capability('gradereport/quizhistory:view', $context);
require_capability('moodle/grade:viewall', $context);

$PAGE->set_pagelayout('popup');
$url = new moodle_url('/grade/report/quizhistory/selectquizzes.php', array('id' => $courseid));
$PAGE->set_url($url);
$PAGE->set_context($context);

// Load jquery and select/deselect script.
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot . "/grade/report/quizhistory/selectquizscript.js"));

echo $OUTPUT->header();

// WARNING : we consider here that one grade_item == one quiz. Actually Moodle support many grade_items, but nowhere in the UI quiz are displayed with 2 grades or more,
//           so it is ok to consider a unique quiz id always match one unique grade item id. For the info, multiple grade item are supported in workshop (as far as I know)
$quizzes = $DB->get_records_select_menu('grade_items', 'itemtype = :itemtype AND itemmodule = :itemmodule',
    array('courseid' => $course->id, 'itemtype' => 'mod', 'itemmodule' => 'quiz'), 'itemname ASC', 'id, itemname');
$selectedquizzes = quizhistoryreport_get_user_preferences('grade_report_quizhistoryquizzes_' . $courseid);
$selectedquizzes = unserialize($selectedquizzes);

$params = array('course' => $course, 'quizzes' => $quizzes, 'selectedquizzes' => $selectedquizzes);
$mform = new \gradereport_quizhistory\selectquizzes_form(null, $params);
$filters = array();
if ($data = $mform->get_data()) {
    $selectedquizzes = array();
    foreach ($data as $quizname => $enabled) {
        if (strpos($quizname, 'quiz') === 0 and $enabled) {
            $selectedquizzes[$quizname] = $enabled;
        }
    }
    // Save the selected quizzes in the user preferences.
    quizhistoryreport_set_user_preference('grade_report_quizhistoryquizzes_' . $courseid, serialize($selectedquizzes));
    close_window();

} else {
    $data = array(
        'course' => $course,
        'quizzes' => $quizzes,
        'selectedquizzes' => $selectedquizzes
    );

    // Set the already selected quiz in the form when opening the popup.
    if (!empty($selectedquizzes)) {
        foreach ($selectedquizzes as $selectedquiz => $enabled) {
            $data[$selectedquiz] = $enabled;
        }
    }
}

$mform->set_data($data);

echo $OUTPUT->heading(get_string('selectquizzes', 'gradereport_quizhistory'));

// select/deselect all
echo '<a href="" moodlesection="all" id="selectallsections">Select</a> / <a href="" moodlesection="all" id="deselectallsections">Deselect All</a><br/><br/>';

$mform->display();

echo $OUTPUT->footer();