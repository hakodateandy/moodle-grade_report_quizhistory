<?php

/**
 * Redirect a quizz attempt knowing the user and quiz.
 *
 * @package gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2015 Hakodate Future University
 */

require('../../../config.php');

require_login();

$cmid = required_param('cmid', PARAM_INT);
$userid = required_param('userid', PARAM_INT);

$PAGE->set_context(context_system::instance());
$PAGE->set_url('/grade/report/quizhistory/redirect_to_attempt.php');

$cm = get_coursemodule_from_id('quiz', $cmid);

// find the best graded attempt for the user.
$quizattempt = $DB->get_records('quiz_attempts', array('quiz' => $cm->instance, 'userid' => $userid), 'sumgrades DESC', '*', 0, 1);
$quizattempt = array_pop($quizattempt);

$redirecturl = $CFG->httpswwwroot . '/mod/quiz/review.php?attempt=' . $quizattempt->id;

redirect($redirecturl);