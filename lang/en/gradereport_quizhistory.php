<?php

/**
 * Strings for component 'gradereport_quizhistory', language 'en'
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allgradeitems'] = 'All grade items';
$string['allgraders'] = 'All graders';
$string['allpassed'] = 'All terms passed';
$string['datefrom'] = 'Date from';
$string['dateto'] = 'Date to';
$string['datetime'] = 'Date and time';
$string['deleteditemid'] = 'Delete item with id {$a}';
$string['errajaxsearch'] = 'Error when searching users';
$string['errornoquizzes'] = 'You must select at least one quiz';
$string['eventgradereportviewed'] = 'Quiz grade history report viewed';
$string['excluded'] = 'Excluded from calculations';
$string['exportfilename'] = 'grade_quizhistory';
$string['foundoneuser'] = '1 user found';
$string['foundnusers'] = '{$a} users found';
$string['feedbacktext'] = 'Feedback text';
$string['finishselectingusers'] = 'Finish selecting users';
$string['gradenew'] = 'Revised grade';
$string['gradeold'] = 'Original grade';
$string['grader'] = 'Grader';
$string['quizhistory:view'] = 'View the quiz grade history';
$string['quizhistoryperpage'] = 'Quiz history entries per page';
$string['quizhistoryperpage_help'] = 'This setting determines the number of quiz history entries displayed per page in the quiz history report.';
$string['loadmoreusers'] = 'Load more users...';
$string['passed'] = 'Passed';
$string['failed'] = 'Failed';
$string['pluginname'] = 'Quiz grade history';
$string['perpage'] = 'Per page';
$string['preferences'] = 'Quiz grade history preferences';
$string['savepreferences'] = 'Save';
$string['seeselectquizzes'] = 'see/select quizzes';
$string['selectuser'] = 'Select user';
$string['selectusers'] = 'Select users';
$string['selectquizzes'] = 'Select quizzes';
$string['selectedusers'] = 'Selected users';
$string['selectedquizzes'] = 'Selected quizzes';
$string['source'] = 'Source';
$string['useractivitygrade'] = '{$a} grade';
$string['useractivityfeedback'] = '{$a} feedback';
