<?php

/**
 * Upgrade code for install
 *
 * @package   grade_report_quizhistory
 * @copyright 2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * upgrade this grade report instance - this function could be skipped but it will be needed later
 * @param int $oldversion The old version of the grade report
 * @return bool
 */
function xmldb_gradereport_quizhistory_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

     if ($oldversion < 2014111001) {

        // Define table report_quizhistory_userpref to be created.
        $table = new xmldb_table('report_quizhistory_userpref');

        // Adding fields to table report_quizhistory_userpref.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('value', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table report_quizhistory_userpref.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table report_quizhistory_userpref.
        $table->add_index('index_name', XMLDB_INDEX_UNIQUE, array('name'));

        // Conditionally launch create table for report_quizhistory_userpref.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Quizhistory savepoint reached.
        upgrade_plugin_savepoint(true, 2014111001, 'gradereport', 'quizhistory');
    }

    if ($oldversion < 2014111002) {

         // Define index index_name (unique) to be dropped form report_quizhistory_userpref.
        $table = new xmldb_table('report_quizhistory_userpref');
        $index = new xmldb_index('index_name', XMLDB_INDEX_UNIQUE, array('name'));

        // Conditionally launch drop index index_name.
        if ($dbman->index_exists($table, $index)) {
            $dbman->drop_index($table, $index);
        }

        // Define field userid to be added to report_quizhistory_userpref.
        $table = new xmldb_table('report_quizhistory_userpref');
        $field = new xmldb_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'value');

        // Conditionally launch add field userid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Quizhistory savepoint reached.
        upgrade_plugin_savepoint(true, 2014111002, 'gradereport', 'quizhistory');
    }


    return true;
}
