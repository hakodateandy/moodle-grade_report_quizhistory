<?php

/**
 * Version details for the grade quizhistory
 *
 * @package    gradereport_quizhistory
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014120300;
$plugin->requires  = 2014110400;
$plugin->component = 'gradereport_quizhistory';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = '1.0.0 (Build 2014120300)';